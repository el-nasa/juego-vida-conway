import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;


public class PanelVida extends JPanel implements ActionListener, MouseListener, MouseMotionListener, KeyListener{

    private Celula celulas = new Celula();

    private int initial = -1;
    private Timer time;

    public PanelVida(){
        setSize(celulas.xPanel, celulas.yPanel);
        setLayout(null);

        addMouseMotionListener(this);
        addMouseListener(this);
        addKeyListener(this);
        setFocusable(true);

        setBackground(Color.BLACK);
        time = new Timer(80, this);

    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        grid(g);
        display(g);

    }

    private void grid(Graphics g){
        g.setColor(Color.darkGray);
        for (int i = 0; i< celulas.membrana.length; i++){
            g.drawLine(0, i* celulas.tam, celulas.xPanel, i* celulas.tam); // dibuja las filas
            g.drawLine(i* celulas.tam, 0, i* celulas.tam, celulas.yPanel); // dibuja las columnas
        }
    }


    private void display(Graphics g){
        g.setColor(Color.CYAN);
        celulas.copiarMembrana();


        for(int x = 0; x< celulas.membrana.length; x++){
            for(int y = 0; y< (celulas.yAlto); y++){
                if(celulas.membrana[x][y] == 1)
                    g.fillRect(x* celulas.tam,y* celulas.tam, celulas.tam, celulas.tam);
            }
        }
    }



    public void actionPerformed(ActionEvent e){
        int alive;

        for(int x = 0; x< celulas.membrana.length; x++){
            for(int y = 0; y< (celulas.yAlto); y++){
                alive = celulas.verificar(x,y);

                if(alive == 3){
                    celulas.membranaSig[x][y]=1;
                }
                else if(alive==2 && celulas.membrana[x][y] == 1){
                    celulas.membranaSig[x][y] = 1;
                }
                else{
                    celulas.membranaSig[x][y] =0;
                }
            }

        }

        repaint();
    }

    public void mouseDragged(MouseEvent e){
        int x = e.getX()/ celulas.tam;
        int y = e.getY()/ celulas.tam;

        if(celulas.membrana[x][y]==0 && initial==0){
            celulas.membranaSig[x][y]=1;
        }
        else if(celulas.membrana[x][y]==1 && initial==1){
            celulas.membranaSig[x][y]=0;
        }
        repaint();
    }

    public void mouseMoved(MouseEvent e){}

    public void mouseClicked(MouseEvent e){}

    public void mousePressed(MouseEvent e){


        int x = e.getX()/ celulas.tam;
        int y = e.getY()/ celulas.tam;

        if(celulas.membrana[x][y]==0){
            initial=0;
        }
        else {
            initial=1;
        }
        repaint();
    }

    public void mouseReleased(MouseEvent e){
        initial=-1;
    }

    public void mouseEntered(MouseEvent e){}

    public void mouseExited(MouseEvent e){}

    public void keyPressed(KeyEvent e){
        int code = e.getKeyCode();

        if(code==e.VK_R){
            celulas.aparecer();
            time.start();
        }
        else if(code == e.VK_C){
            celulas.limpiar();
            time.stop();
        }
        else if(code==e.VK_S){
            time.start();
        }

        else if(code==e.VK_A){
            time.stop();
        }

        repaint();
    }

    public void keyTyped(KeyEvent e){}

    public void keyReleased(KeyEvent e){}
}
