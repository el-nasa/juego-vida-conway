import javax.swing.JFrame;


public class Ventana extends JFrame{
    public Ventana(){

        add(new PanelVida());
        setSize(1300, 700);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Sirve para que qal cerrar la ventana, acabe todos los procesos
    }

    public static void main(String[] args){
        new Ventana();
    }
}
