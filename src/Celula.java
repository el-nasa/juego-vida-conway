public class Celula {
    int xPanel = 1300;
    int yPanel = 700;
    int tam = 10;
    int xAncho = xPanel/ tam;
    int yAlto = yPanel/ tam;


    int[][] membrana = new int[xAncho][yAlto];
    int[][] membranaSig = new int[xAncho][yAlto];

    protected void aparecer(){
        for(int x = 0; x< membrana.length; x++){
            for(int y = 0; y< (yAlto); y++){
                if((int)(Math.random()*5)==0){
                    membranaSig[x][y] = 1;
                }
            }
        }
    }

    protected void copiarMembrana() {
        for (int x = 0; x < membrana.length; x++) {
            for (int y = 0; y < (yAlto); y++) {
                membrana[x][y] = membranaSig[x][y];
            }
        }
    }

    protected int verificar(int x, int y){
        int conVida = 0;
        conVida += membrana[(x + xAncho -1) % xAncho][(y + yAlto -1) % yAlto];
        conVida += membrana[(x + xAncho) % xAncho][(y + yAlto -1)  % yAlto];

        conVida += membrana[(x + xAncho +1)% xAncho][(y + yAlto -1) % yAlto];
        conVida += membrana[(x + xAncho -1) % xAncho][(y + yAlto)  % yAlto];

        conVida += membrana[(x + xAncho +1) % xAncho][(y + yAlto)  % yAlto];
        conVida += membrana[(x + xAncho -1) % xAncho][(y + yAlto +1)  % yAlto];

        conVida += membrana[(x + xAncho) % xAncho][(y + yAlto +1)  % yAlto];
        conVida += membrana[(x + xAncho +1) % xAncho][(y + yAlto +1)  % yAlto];

        return conVida;
    }

    protected void limpiar(){
        for (int x = 0; x < membrana.length; x++) {
            for (int y = 0; y < (yAlto); y++) {
                membranaSig[x][y]=0;
            }
        }
    }


}

